//
//  SMBookModel.m
//  Mobile Book Search
//
//  Created by Siemian on 03.07.2014.
//  Copyright (c) 2014 siemian. All rights reserved.
//

#import "SMBookModel.h"

@implementation SMBookModel




-(SMBookModel*)initWithDictionary:(NSDictionary*)dict;
{
    self.title=[dict objectForKey:@"title"];
    self.notes=[dict objectForKey:@"notes"];
    
    NSArray *authors=[dict objectForKey:@"author_data"];
    
    if(authors.count!=0)
    {
        self.author=@"";
        for(NSDictionary *one in authors)
        {
            self.author=[self.author stringByAppendingFormat:@"%@; ",[one objectForKey:@"name"]];
        }
    }
    else
        self.author=[dict objectForKey:@"Unknown"];
    
    return self;
}
@end
