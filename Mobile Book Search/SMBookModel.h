//
//  SMBookModel.h
//  Mobile Book Search
//
//  Created by Siemian on 03.07.2014.
//  Copyright (c) 2014 siemian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMBookModel : NSObject
@property(strong) NSString *title;
@property(strong) NSString *author;
@property(strong) NSString *notes;

-(SMBookModel*)initWithDictionary:(NSDictionary*)dict;

@end
