//
//  SMMainViewController.h
//  Mobile Book Search
//
//  Created by Siemian on 03.07.2014.
//  Copyright (c) 2014 siemian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMMainViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
