//
//  SMDataService.m
//  Mobile Book Search
//
//  Created by Siemian on 03.07.2014.
//  Copyright (c) 2014 siemian. All rights reserved.
//

#import "SMDataService.h"
#import <AFHTTPRequestOperationManager.h>

@interface SMDataService()

@property(strong) AFHTTPRequestOperationManager *manager;

@end

@implementation SMDataService

+ (id)sharedInstance {
    static SMDataService *sharedI = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedI = [[self alloc] init];
    });
    sharedI.manager=[[AFHTTPRequestOperationManager alloc] init];
    AFHTTPResponseSerializer *respser=[[AFHTTPResponseSerializer alloc]init];
    NSMutableSet *sett=[respser.acceptableContentTypes mutableCopy];
    [sett addObject:@"text/html"];//isbndb is returning json with content type text/html
    respser.acceptableContentTypes=sett;
    sharedI.manager.responseSerializer=respser;
    
    return sharedI;
}



-(void)getBooksWithSearhPhrase:(NSString*)phrase successBlock:(void(^)(NSDictionary *dict))succesBlock
{
    
    NSString *url=[NSString stringWithFormat:@"http://isbndb.com/api/v2/json/U3W26R0N/books?q=%@",phrase];
    
    [_manager GET:url
            parameters:nil
           success:^(AFHTTPRequestOperation *operation, id responseObject) {
               NSDictionary *array = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
               succesBlock(array);
           } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
               succesBlock(nil);
           }];
}
@end
