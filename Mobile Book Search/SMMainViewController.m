//
//  SMMainViewController.m
//  Mobile Book Search
//
//  Created by Siemian on 03.07.2014.
//  Copyright (c) 2014 siemian. All rights reserved.
//

#import "SMMainViewController.h"
#import "SMTBookCell.h"
#import "SMDataService.h"
#import "SMBookModel.h"
#import "SMDetailViewController.h"
#import <SVProgressHUD.h>

enum CellType : NSInteger {
    kSearchResult,
    kRegular
};

@interface SMMainViewController ()<UISearchBarDelegate>
@property(strong) NSMutableArray *items;
@property(strong) NSMutableArray *searchItems;

@end

@implementation SMMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self downloadSampleData];
}

#pragma mark download data
-(void)downloadDataWithPhrase:(NSString*)phrase
{
    [self showProgressDialog];
 [[SMDataService sharedInstance] getBooksWithSearhPhrase:phrase successBlock:^(NSDictionary *dict) {
     
     _searchItems=[[NSMutableArray alloc] init];
     for(NSDictionary *one in [dict objectForKey:@"data"])
     {
         [_searchItems addObject:[[SMBookModel alloc] initWithDictionary:one]];
     }
    [self hideProgressDialog];
     [self.searchDisplayController.searchResultsTableView reloadData];
 }];
    
}

-(void)downloadSampleData
{
    [self showProgressDialog];
    [[SMDataService sharedInstance] getBooksWithSearhPhrase:@"science" successBlock:^(NSDictionary *dict) {
        
        _items=[[NSMutableArray alloc] init];
        for(NSDictionary *one in [dict objectForKey:@"data"])
        {
            [_items addObject:[[SMBookModel alloc] initWithDictionary:one]];
        }
        [self hideProgressDialog];
        [self.tableView reloadData];
        
    }];
}

#pragma mark searchbar

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self downloadDataWithPhrase:searchBar.text];
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    _searchItems=nil;
    [self.searchDisplayController.searchResultsTableView reloadData];
}


#pragma mark tableView
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 106;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==self.tableView)
        return _items.count;
    else
        return _searchItems.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    SMTBookCell *cell=[self.tableView dequeueReusableCellWithIdentifier:@"bookCell"];
    
    if(!cell)
        cell=[[SMTBookCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"bookCell"];
    
    SMBookModel *book;
    if(tableView==self.tableView)
    {
        book=((SMBookModel*)_items[indexPath.row]);
        cell.tag=kRegular;
        
    }
    else
    {
        book=((SMBookModel*)_searchItems[indexPath.row]);
        cell.tag=kSearchResult;
    }
    cell.titleLabel.text=book.title;
    cell.authorLabel.text=book.author;
    
    
    return cell;
}


#pragma mark segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"showDetails"])
    {
        SMTBookCell *cell=((SMTBookCell*)sender);
        SMDetailViewController *dst=segue.destinationViewController;
        NSIndexPath *ip;
        if(cell.tag==kRegular)
        {
            ip=[self.tableView indexPathForCell:cell];
            dst.book=_items[ip.row];
        }
        else
        {
            ip=[self.searchDisplayController.searchResultsTableView indexPathForCell:cell];
            dst.book=_searchItems[ip.row];
        }
        
        
        
            
    }
}

#pragma mark progress hud
-(void)showProgressDialog
{
    [SVProgressHUD show];
   
}

-(void)hideProgressDialog
{
  
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
       
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
    });
}


@end
