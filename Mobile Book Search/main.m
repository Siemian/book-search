//
//  main.m
//  Mobile Book Search
//
//  Created by Siemian on 03.07.2014.
//  Copyright (c) 2014 siemian. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SMAppDelegate class]));
    }
}
