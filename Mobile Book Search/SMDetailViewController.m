//
//  SMDetailViewController.m
//  Mobile Book Search
//
//  Created by Siemian on 03.07.2014.
//  Copyright (c) 2014 siemian. All rights reserved.
//

#import "SMDetailViewController.h"

@interface SMDetailViewController ()

@end

@implementation SMDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    _titleLabel.text=_book.title;
    _author.text=_book.author;
    _notes.text=_book.notes;
}

@end
