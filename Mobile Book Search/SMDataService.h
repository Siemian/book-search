//
//  SMDataService.h
//  Mobile Book Search
//
//  Created by Siemian on 03.07.2014.
//  Copyright (c) 2014 siemian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMDataService : NSObject
+ (id)sharedInstance;

-(void)getBooksWithSearhPhrase:(NSString*)phrase successBlock:(void(^)(NSDictionary *dict))succesBlock;

@end
